
dataset <- dataset_odre(x = "consommation-annuelle-brute-regionale")
obj1 <- prep_dju(reg = "52", an = 2020, coeff_dju = 25)
obj2 <- prep_conso(dataset_conso = dataset$dataset, reg = "52", an = 2020, coeff_fce_reg = 1/17)
obj3 <- finit_conso(df_conso = obj2, df_dju = obj1)
obj4 <- table_conso(df = obj3, an = 2020)

test_that("prep_dju fonctionne", {
  expect_error(prep_dju(reg = "52", an = 2020, coeff_dju = 25, url_dju_nat = "www.google.com", url_dju_reg = "www.google.com"))
  expect_true(is.data.frame(obj1))
  expect_true(ncol(obj1)==6)
})

test_that("prep_conso fonctionne", {
  expect_true(is.data.frame(obj2))
  expect_true(ncol(obj2)==7)
})

test_that("finit_conso works", {
  testthat::expect_true(is.data.frame(obj3))
  testthat::expect_true(nrow(obj3) == nrow(obj1) + nrow(obj2))
})

test_that("viz_conso works", {
  objet <- viz_conso(df = obj3, coeff_fce_reg = 1/17)
  expect_true(any(grepl("gg", attr(objet, "class"))))
  expect_true(any(grepl("ggplot", attr(objet, "class"))))
})

test_that("table_conso works", {
  expect_true(is.data.frame(obj4))
})

test_that("table_conso_viz works", {
  expect_is(table_conso_viz(obj4),  "flextable")
})
