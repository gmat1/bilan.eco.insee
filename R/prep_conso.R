#' prep_conso : preparation des consommations electriques et de gaz France, region observee
#'
#' @description les consommations nationales sont France metropolitaine pour l'electricite et France continentale pour le gaz
#' @param dataset_conso le dataset "consommation-annuelle-brute-regionale" telecharge sur la plateforme ODRE et mis en forme avec `dataset_odre()`
#' @param reg le code region au format texte
#' @param an l'annee observee au format numerique
#' @param coeff_fce_reg un coefficient qui permet de ramener les consommations nationales a l'echelle des consommations regionales, 1/17 par exemple pour les Pays de la Loire
#'
#' @return un dataframe presentantles consommations pour la France et la region observee, depuis 2013, qui attend sa fusion avec les dju avant dataviz
#' @importFrom dplyr select rename group_by summarise mutate bind_rows filter if_else case_when
#' @importFrom tidyr pivot_longer
#' @importFrom rlang .data
#' @export
#'
#' @examples
#' \dontrun{
#' prep_conso(dataset_conso = conso$dataset, reg = "52", an = 2020, coeff_fce_reg = 1/17)
#' }  

prep_conso <- function(dataset_conso, reg = "52", an = 2020, coeff_fce_reg = 1/17) {
  
  conso_df <- dataset_conso %>% 
    dplyr::select(.data$annee, .data$code_insee_region, .data$region, gaz = .data$consommation_brute_gaz_totale, 
                  elec = .data$consommation_brute_electricite_rte)
  
  
  conso_df_fr <- conso_df %>% 
    dplyr::group_by(.data$annee) %>% 
    dplyr::summarise(gaz = sum(.data$gaz, na.rm = TRUE),
                     elec = sum(.data$elec, na.rm = TRUE)) %>% 
    dplyr::mutate(region = "France") %>% 
    dplyr::bind_rows(dplyr::filter(conso_df, .data$code_insee_region == reg), .) %>% 
    dplyr::select(-.data$code_insee_region) %>% 
    tidyr::pivot_longer(cols = c(.data$gaz, .data$elec), names_to = "nrj", values_to = "conso_TWh") %>% 
    dplyr::mutate(
      conso_TWh = .data$conso_TWh/1000,
      conso_comp = dplyr::if_else(.data$region == "France", .data$conso_TWh * coeff_fce_reg, .data$conso_TWh),
      region2 = dplyr::case_when(
        .data$region == "France" & .data$nrj == "gaz" ~ "France continentale",
        .data$region == "France" & .data$nrj == "elec" ~ "France m\u00e9tropolitaine",
        TRUE ~ .data$region),
      region3 = dplyr::if_else(.data$region == "France", "2FR", "1reg"),
      nrj = dplyr::if_else(.data$nrj == "gaz", .data$nrj,  "\u00e9lectricit\u00e9"),
      dplyr::across(tidyselect::vars_select_helpers$where(is.numeric), ~ round(.x, 2))
    ) 
  conso_df_fr
}
