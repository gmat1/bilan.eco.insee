#' regions
#'
#' codes et libelles des regions francaises.
#'
#' @format A data frame with 18 rows and 2 variables:
#' \describe{
  #'   \item{ REG }{  code region factor }
  #'   \item{ zone }{ libelle de la region factor }
  #' }
  #' @source Source
  "regions"
