# bilan.eco.insee

master : [![pipeline status](https://gitlab.com/dreal-datalab/bilan.eco.insee/badges/master/pipeline.svg)](https://gitlab.com/dreal-datalab/bilan.eco.insee/-/commits/master)[![coverage report](https://gitlab.com/dreal-datalab/bilan.eco.insee/badges/master/coverage.svg)](https://gitlab.com/dreal-datalab/bilan.eco.insee/-/commits/master)[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)


Scipt de collecte et de mise en page des pages climat air énergie du bilan économique et social annuel de l'INSEE.


Le site web de présentation du package est accessible via : <a href="https:/dreal-datalab.gitlab.io/bilan.eco.insee/index.html">https://dreal-datalab.gitlab.io/bilan.eco.insee/</a>.


Le but de {bilan.eco.insee} est de faciliter la production de la page climat, air, énergie d'un bilan économique et social régional annuel. 
Il contient un canevas de rapport .docx paramétrable selon la région d’observation et l'année obervée, incluant les fonctions nécessaires à la création des illustrations. Ce canevas reste à commenter.
Le package contient également un rapport html, lui aussi paramétrable, présentant les sources de données, la documentation à rassembler et des tables d'indicateurs nécessaires à la prodution des commentaires d'analyses, cf <a href="https://dreal-datalab.gitlab.io/bilan.eco.insee/articles/exploration.html">vignette exploration</a>.

Vous pouvez installer {bilan.eco.insee} depuis gitlab avec :

``` r
remotes::install_gitlab(repo = "dreal-datalab/bilan.eco.insee", build_vignettes = TRUE, dependencies = TRUE)
```
L'option `build_vignette` doit être fixée à `TRUE` pour que le rapport d'indicateurs complémentaires soit disponible.

Il est recommander de fixer l'option `dependencies` à `TRUE` pour que les packages nécessaires à la compilation des rapports soit installés si manquants.

Vous pouvez accedéer aux rapports à paramétrer avec la fonction `edit_rapports()` :

``` r
edit_rapports(repo = "nom_du_repertoire_de_travail_souhaite", changer_rep_travail = TRUE)
```
Cette fonction va créer le dossier `repo`, y copier les deux rapports à paramétrer et les ouvrir. 
Le paramètre `changer_rep_travail` fixé à `TRUE` permet de définir ce dossier comme répertoire de travail.


